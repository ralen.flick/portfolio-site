import { Link } from "react-router-dom";
import { Tabs } from "@mui/material";
import { Tab } from "@mui/material";
import { useState } from "react";

const Navigation = () => {
  const routes = ["/Home", "/About"];

  const { page, setPage } = useState(0);

  const handleChange = (event, newValue) => {
    setPage(newValue);
  };

  return (
    <div>
      <Tabs value={page} onChange={handleChange}>
        <Tab label="Home" value={routes[0]} />
        <Tab label="About" value={routes[1]} />
      </Tabs>
    </div>
  );
};

export default Navigation;
